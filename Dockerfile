FROM alpine:3

RUN apk add --no-cache bash ffmpeg python3 py3-pip
RUN pip install Flask

RUN mkdir /usr/src/app -p
WORKDIR /usr/src/app/

ADD . /usr/src/app/
CMD bash run.sh